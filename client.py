import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
import argparse
import flwr as fl
import numpy as np
import tensorflow as tf
from helpers.CNN import CNNModel
from helpers.DataPreprocessornew import DataPreprocessornew
from sklearn.metrics import f1_score, precision_score, recall_score
import tensorflow_privacy as tfp
from tfp.privacy.analysis import compute_dp_sgd_privacy

# Make TensorFlow logs less verbos

# Create a ECG client for Federated Learning:
class ECGClient(fl.client.NumPyClient):
    def __init__(self, traindata, testdata, valdata, input_shape):
        self.traindata = traindata
        self.testdata = testdata
        self.valdata = valdata
        self.input_shape = input_shape
        self.model = CNNModel.create_model(input_shape)

    def get_properties(self, config):
        """Get properties of client."""
        raise Exception("Not implemented")

    def get_parameters(self):
        """Get parameters of the local model."""
        raise Exception("Not implemented (server-side parameter initialization)")
    
    def fit(self, parameters, config):
        """Train parameters on the locally held training set."""
        
        # Update local model parameters:
        self.model.set_weights(parameters)
        
        # Get hyperparameters for this round
        batch_size: int = config["batch_size"]
        epochs: int = config["local_epochs"]
        
        # Train the model
        history = self.model.fit(
            self.traindata, 
            epochs=epochs, 
            batch_size=batch_size,
            validation_data=self.valdata)
        
        # Return updated model parameters and results
        weights = self.model.get_weights()
        results = {
            "loss": history.history["loss"][0],
            "accuracy": history.history["categorical_accuracy"][0],
            "val_loss": history.history["val_loss"][0],
            "val_accuracy": history.history["val_categorical_accuracy"][0],
        }       
        num_examples_train = len(self.traindata)
        return weights, num_examples_train, results
    
    def evaluate(self, parameters, config):
        # Upate local model with global model parameters:
        self.model.set_weights(parameters)

        # Evaluation:
        y_values = np.concatenate([y for x, y in self.testdata], axis=0)
        y_values = np.argmax(y_values, axis=1)
        loss, accuracy = self.model.evaluate(self.testdata)
        num_examples_test = len(y_values)
        y_pred = self.model.predict(self.testdata)
        y_pred = np.argmax(y_pred, axis=1)
        f1 = f1_score(y_values, y_pred, average='macro')
        precision = precision_score(y_values, y_pred, average='macro')
        recall = recall_score(y_values, y_pred, average='macro')

        results = {
            "accuracy": accuracy,
            "precision": precision,
            "recall": recall,
            "f1": f1,
        }
        print("-"*100)
        print("Test loss:", loss)
        print("-"*100)
        return loss, num_examples_test, {"accuracy": accuracy}
    

def main() -> None:
    parser = argparse.ArgumentParser(description="FlowerClients")
    parser.add_argument("--hospital", type=int, required=True)
    #parser.add_argument("--augmentation", type=bool, required=True)
    args = parser.parse_args()

    # Get the hospital number:
    hospital = args.hospital
    #augmentation = args.augmentation

    # Do the preprocessing of the data:
    traindata, testdata, valdata = DataPreprocessornew.load_dataset(batch_size=64, federated=True, hospital=hospital, augmentation=True)
    client = ECGClient(traindata, testdata, valdata, (5000, 12))

    fl.client.start_numpy_client(
        server_address="127.0.0.1:8980",
        client=client
    )

if __name__ == "__main__":
    main()
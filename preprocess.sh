#!/bin/bash

# Define the path to your data directory
path="/home/dennis/GitHub/ECG/data"

# Preprocessing for Hospital 1
echo "Starting preprocessing for Hospital 1..."
python -c "from helpers.DataPreprocessornew import DataPreprocessornew; DataPreprocessornew.create_dataset('$path', federated=True, hospital=1)"
echo "Finished preprocessing for Hospital 1"

# Preprocessing for Hospital 2
echo "Starting preprocessing for Hospital 2..."
python -c "from helpers.DataPreprocessornew import DataPreprocessornew; DataPreprocessornew.create_dataset('$path', federated=True, hospital=2)"
echo "Finished preprocessing for Hospital 2"

# Preprocessing for Hospital 3
echo "Starting preprocessing for Hospital 3..."
python -c "from helpers.DataPreprocessornew import DataPreprocessornew; DataPreprocessornew.create_dataset('$path', federated=True, hospital=3)"
echo "Finished preprocessing for Hospital 3"

# Preprocessing for the Centralized Server
echo "Starting preprocessing for Centralized Server..."
python -c "from helpers.DataPreprocessornew import DataPreprocessornew; DataPreprocessornew.create_dataset('$path', federated=False)"
echo "Finished preprocessing for Centralized Server"

echo "All preprocessing finished!"

import tensorflow as tf
import os 

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'

(x_train, y_train), (x_test, y_test) = tf.keras.datasets.cifar10.load_data()
print(len(x_train))
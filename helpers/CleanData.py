import os
import math
import wfdb
import pandas as pd
from scipy.signal import resample
import numpy as np
import scipy.io as sio


class CleanData:
    """
    This class cleans the data and saves it to the disk.
    It specifically needs to be run before the data is processed. Needs to be set in a specific format
    and the files most be ordered correctly.
    """

    @staticmethod
    def remove_unwanted_files(file_path, label_code):
        """
        Checks if the file is labeled correctly.
        :param file_path: path to the file
        :param label_code: label to check for
        :return: True if the file is labeled correctly, False otherwise
        """
        if file_path.endswith('.hea'):
            with open(file_path, 'r') as f:
                header_lines = f.readlines()
                for line in header_lines:
                    if "dx" in line.lower():
                        dx_field = line.split(":")[1]
                labels = dx_field.split(',')
                labels = [int(label) for label in labels]
                if label_code == 'AF':
                    if 164890007 in labels or 164889003 in labels:
                        return True
                elif label_code == 'OTHER':
                    if 164890007 not in labels and 164889003 not in labels and 426783006 not in labels:
                        return True
                else:
                    if labels.count(426783006) == len(labels):
                        return True

    @staticmethod
    def get_files_to_clean(root_dir, label=None, folder=None):
        """
        Cleans files for further processing.
        :param folder: If a specific folder is given, only files in that folder will be cleaned
        :param root_dir: path to the root folder
        :param label: label to check for (if None, all files will be cleaned) AF or SR
        :return: list of cleaned files
        """

        # Get the file paths of every file in the root folder:
        file_paths = []

        # Get the file paths of every file in the root folder and subfolders ending with .hea:
        for root, dirs, files in os.walk(root_dir):
            for file in files:
                # Check in which folder the file is:
                if file.endswith('.hea'):
                    file_paths.append(os.path.join(root, file))

        # Kick those .hea files out of the list, where no .mat file exists:
        for file in file_paths:
            if not os.path.exists(file[:-4] + '.mat'):
                file_paths.remove(file)

        # Get the files to clean:
        files_to_clean = []

        if label is None:
            # Remove all the .hea files:
            files_to_clean = file_paths
        else:
            # Remove all files that do not have the correct label:
            for file in file_paths:
                if CleanData.remove_unwanted_files(file, label):
                    files_to_clean.append(file)

        return files_to_clean

    @staticmethod
    def clean(header_files, output_dir):
        """
        Cleans all the files in the header_files list and saves them to the output directory as .csv files.
        :param header_files: list of header files to clean
        :param output_dir: path to the output directory
        :return: None
        """
        # Define parameters
        target_fs = 500  # in Hz
        segment_length = 10  # in seconds
        label_name = ""

        # Loop through each record and extract the ECG data
        for hea_file in header_files:

            # Check if the file already exists in either subfolder: AF, SR or OTHER
            if os.path.exists(os.path.join(output_dir, "AF", hea_file.split('/')[-1][:-4] + '.csv')) or \
                    os.path.exists(os.path.join(output_dir, "SR", hea_file.split('/')[-1][:-4] + '.csv')) or \
                    os.path.exists(os.path.join(output_dir, "OTHER", hea_file.split('/')[-1][:-4] + '.csv')):
                continue

            print("Cleaning file: " + hea_file)

            # Read the header information
            try:
                header = wfdb.rdheader(hea_file[:-4])
            except:
                print("Could not read header file: " + hea_file)
                continue

            # Read the header information for labels
            with open(hea_file) as f:
                header_lines = f.readlines()
                for line in header_lines:
                    if "dx" in line.lower():
                        dx_field = line.lower()
                labels = dx_field.split(':')[1].split(',')
                labels = [int(label) for label in labels]
                f.close()

            if 164890007 in labels or 164889003 in labels:
                label_name = "AF"
            elif labels.count(426783006) == len(labels):
                label_name = "SR"
            # Kick out all files that contain either
            elif 426783006 not in labels and 164890007 not in labels and 164889003 not in labels:
                label_name = "OTHER"
            else:
                continue

            # Load the signal data from the corresponding MATLAB file
            mat_file = os.path.join(hea_file.replace('.hea', '.mat'))
            mat_data = sio.loadmat(mat_file)
            signal_data = mat_data['val']
            sample_frequency = header.fs
            signal_length = signal_data.shape[1] / sample_frequency

            # Resample the ECG data to the target sampling frequency:
            if sample_frequency != target_fs and signal_length == segment_length:
                resampled_data = resample(signal_data, int(target_fs * segment_length), axis=1)
            else:
                resampled_data = signal_data

            # Throw out samples that are shorter than 10 seconds:
            if signal_length < segment_length:
                continue

            # Segment the signal into 10 second segments if it is longer than 10 seconds (no overlap)
            if signal_length > segment_length:
                # Calculate the number of segments:
                num_segments = math.floor(signal_length / segment_length)
                # Loop through each segment and save it to a separate CSV file:
                for segment in range(num_segments):
                    start_idx = segment * int(segment_length * sample_frequency)
                    end_idx = (segment + 1) * int(segment_length * sample_frequency)
                    signal_segment = signal_data[:, start_idx:end_idx]
                    # Resample the ECG data to the target sampling frequency:
                    if sample_frequency != target_fs:
                        resampled_segment = resample(signal_segment, int(target_fs * segment_length), axis=1)
                    else:
                        resampled_segment = signal_segment
                    try:
                        # Create a DataFrame from the resampled segment
                        df = pd.DataFrame(np.transpose(resampled_segment), columns=[header.sig_name])
                        # Save label in the DataFrame
                        df['label'] = label_name
                        # Write the DataFrame to a CSV file
                        new_dir = output_dir + '/' + label_name
                        output_file = os.path.join(new_dir, header.record_name + f'_segment{segment + 1}.csv')
                        df.to_csv(output_file, index=False)
                    except:
                        print("Could not write to file: " + output_file)
                        continue

            else:
                try:
                    df = pd.DataFrame(np.transpose(resampled_data), columns=[header.sig_name])
                    df['label'] = label_name
                    new_dir = output_dir + '/' + label_name
                    output_file = os.path.join(new_dir, header.record_name + '.csv')
                    df.to_csv(output_file, index=False)
                except:
                    print("Could not write to file: ")
                    continue

    @staticmethod
    def clean_files(root_dir, folder, label=None):
        """
        Cleans files for further processing.
        :param folder: If a specific folder is given, only files in that folder will be cleaned
        (Otherwise all subfolders will be cleaned)
        :param root_dir: path to the root folder
        :param label: label to check for (if None, all files will be cleaned) AF or SR
        :return: list of cleaned files
        """

        # Needs to be a valid folder:
        if folder is None:
            return
        
        # Gets a list of header files to clean:
        files_to_clean = CleanData.get_files_to_clean(os.path.join(root_dir, f'raw/{folder}'), label, folder)

        # Check if output directory exists, if not, create it and the subdirectories:
        output_dir = os.path.join(root_dir, f'cleaned/{folder}')
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
            os.makedirs(os.path.join(output_dir, 'AF'))
            os.makedirs(os.path.join(output_dir, 'SR'))
            os.makedirs(os.path.join(output_dir, 'OTHER'))

        CleanData.clean(files_to_clean, output_dir)
        return True


# Test out the class:
if __name__ == '__main__':
    root_folder = "/Users/dennis/Desktop/data"
    CleanData.clean_files(root_folder, folder="WFDBRecords")
    # Read JS00008.mat and print it out

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
import random
import warnings
import pandas as pd
import numpy as np
import tensorflow as tf
from helpers.AugmentData_tensor import AugmentData


# Ignore warnings:
warnings.filterwarnings('ignore')

class DataPreprocessornew:

    @staticmethod
    def apply_augmentation(features, labels, weights):
        """
        Applies all the data augmentation functions to a given data file.
        :param labels: the labels of the data
        :param features: the features of the data
        """

        # Gets the electrode data:
        try:
            electrode_data = AugmentData.get_electrode_data(features)
            # Randomly choose a suitable snr:
            snr = np.random.uniform(0.1, 0.5)

            # Randomly choose a suitable noise fraction for gaussian noise:
            noise_fraction = np.random.uniform(0.01, 0.1)

            # 10% Chance to apply add_baseline_wander
            # 10% Chance to apply set_random_electrodes_to_zero
            # 80% Chance to apply add_gaussian_noise or add_gaussian_noise_snr
            if np.random.uniform(0, 1) < 0.1:
                lead_data = AugmentData.add_baseline_wander(electrode_data, features)
            elif 0.2 > np.random.uniform(0, 1) > 0.1:
                lead_data = AugmentData.set_random_electrode_to_zero(electrode_data, features)
            else:
                aug_func = np.random.choice([1, 2])
                if aug_func == 1:
                    lead_data = AugmentData.add_gaussian_noise(electrode_data, noise_fraction, features)
                else:
                    lead_data = AugmentData.add_gaussian_noise_snr(electrode_data, snr, features)
        except:
            lead_data = features

        # Check, that lead_data has the same shape as features:
        if lead_data.shape != features.shape:
            lead_data = features


        return lead_data, labels, weights

    @staticmethod
    def parse_function(csv_file):
        """"
        Parses a single example to a feature and label in the float32 format
        :param csv_file: path to the CSV file
        :param sample_weights: dictionary containing sample weights
        :return: features, labels, and sample_weight
        """

        # Get the CSV file path:
        csv_file_str = csv_file.numpy().decode('utf-8')
        # Load the CSV file using pandas:
        data = pd.read_csv(csv_file_str)

        # Get the label:
        labels = data.iloc[0, -1]
        label_dict = {'AF': 0, 'SR': 1, 'OTHER': 2}
        labels = label_dict[labels]
        labels = tf.keras.utils.to_categorical(labels, num_classes=3, dtype='float32')

        # Process Features:
        features = data.iloc[:, :12]
        features = features.astype('float32')

        return features, labels
    
    @staticmethod
    def parse_function_with_weights(csv_file, sample_weights_tensor):

        csv_file_str = csv_file.numpy().decode('utf-8')
        # Load the CSV file using pandas:
        data = pd.read_csv(csv_file_str)

        # Get the label:
        labels = data.iloc[0, -1]
        label_dict = {'AF': 0, 'SR': 1, 'OTHER': 2}
        labels = label_dict[labels]
        labels_categorical = tf.keras.utils.to_categorical(labels, num_classes=3, dtype='float32')

        # Process Features:
        features = data.iloc[:, :12]
        features = features.astype('float32')

        # Get the sample_weight for the current label:
        sample_weight = sample_weights_tensor.numpy()[labels]

        return features, labels_categorical, sample_weight


    @staticmethod
    def create_list_of_dics(data_dir, dir_name='cleaned'):
        """
        Creates a list of hospital files (each hostpital = one dictionary)
        """
        data_dir = os.path.join(data_dir, dir_name)
        list_dict = []
        for folder in os.listdir(data_dir):
            if folder.startswith('.'):
                continue
            folder_files = []
            folder_path = os.path.join(data_dir, folder)
            for subfolder in os.listdir(folder_path):
                if subfolder.startswith('.'):
                    continue
                subfolder_path = os.path.join(folder_path, subfolder)
                for file in os.listdir(subfolder_path):
                    if file.startswith('.'):
                        continue
                    file_path = os.path.join(subfolder_path, file)
                    folder_files.append(file_path)
            list_dict.append({folder: folder_files})
        
        return list_dict
    
    @staticmethod
    def shuffle_files(hospital_files):
        """
        Shuffles the files in the list of files
        """
        # Check if more than one list 
        for file_lists in hospital_files:
            random.seed(42)
            random.shuffle(file_lists)
        return hospital_files
    
    @staticmethod
    def split_data_train_test_validation(hospital_files, train_split, test_split, validation_split, fed=True):
        """
        Splits the data into train, test and validation data
        """
        # Check if the splits are correct:
        if train_split + test_split + validation_split != 1:
            raise ValueError("The splits are not correct")
        
        # Split the data:
        train_data = []
        test_data = []
        validation_data = []
        for file_list in hospital_files:
            train_data.append(file_list[:int(len(file_list)*train_split)])
            test_data.append(file_list[int(len(file_list)*train_split):int(len(file_list)*(train_split+test_split))])
            validation_data.append(file_list[int(len(file_list)*(train_split+test_split)):])
        
        # Flatten the lists:
        train_data = [item for sublist in train_data for item in sublist]
        test_data = [item for sublist in test_data for item in sublist]
        validation_data = [item for sublist in validation_data for item in sublist]
        
        return train_data, test_data, validation_data
    
    @staticmethod
    def calculate_sample_weights(train_files):
        """
        Calculate sample weights based on the training data.
        :param train_files: list of paths to the training files
        :return: sample_weights (dictionary containing sample weights)
        """
        class_counts = {'AF': 0, 'SR': 0, 'OTHER': 0}

        for file in train_files:
            df = pd.read_csv(file)
            label = df.iloc[0, -1]
            class_counts[label] += 1

        # Calculate the total number of samples:
        total_samples = sum(class_counts.values())

        # Compute the class frequencies:
        class_frequencies = {cls: count / total_samples for cls, count in class_counts.items()}

        # Calculate the inverse of the class frequencies:
        inverse_class_frequencies = {cls: 1 / freq for cls, freq in class_frequencies.items()}

        # Normalize the weights, so that the smallest weight is 1:
        min_weight = min(inverse_class_frequencies.values())
        sample_weights = {cls: np.round(weight / min_weight, decimals=2) for cls, weight in
                          inverse_class_frequencies.items()}

        return {0: sample_weights['AF'], 1: sample_weights['SR'], 2: sample_weights['OTHER']}
    

    @staticmethod
    def create_dataset(data_path, federated=False, hospital=None):
        
        # Get the dictionary of files:
        list_dict = DataPreprocessornew.create_list_of_dics(data_path)
      
        # Create a list of lists of files:
        if not federated:
            hospital_files = []
            for hospital_dict in list_dict:
                for key, values in hospital_dict.items():
                    hospital_files.append(values)
         
        elif federated and hospital is not None:
            hospital_files = []
            for hospital_dict in list_dict:
                for key, values in hospital_dict.items():
                    if int(key) == hospital:
                        hospital_files.append(values)
        else:
            raise ValueError("Please specify a hospital for federated learning")
        
        
        # Shuffle the files:
        hospital_files = DataPreprocessornew.shuffle_files(hospital_files)

        # Split the data:
        train_dataset, test_dataset, validation_dataset = DataPreprocessornew.split_data_train_test_validation(hospital_files, 0.8, 0.1, 0.1, fed=federated)

        # Calculate the sample weights for the training data:
        sample_weights = DataPreprocessornew.calculate_sample_weights(train_dataset)
        sample_weights_tensor = tf.constant([sample_weights[0], sample_weights[1], sample_weights[2]], dtype=tf.float32)

        # Create the datasets:
        train_dataset = tf.data.Dataset.from_tensor_slices(train_dataset)
        test_dataset = tf.data.Dataset.from_tensor_slices(test_dataset)
        validation_dataset = tf.data.Dataset.from_tensor_slices(validation_dataset)

        # Parse Training dataset:
        train_dataset = train_dataset.map(
        lambda x: tf.py_function(
            DataPreprocessornew.parse_function_with_weights,
            inp=[x, sample_weights_tensor],
            Tout=(tf.float32, tf.float32, tf.float32),
        ),
        num_parallel_calls=tf.data.AUTOTUNE
        )
        
        # Create a copy of the training dataset for no augmentation:
        train_dataset_no_aug = train_dataset

        # Apply augmentation to the training dataset:
        train_dataset = train_dataset.map(
        lambda x, y, w: tf.py_function(DataPreprocessornew.apply_augmentation, inp=[x, y, w], Tout=(tf.float32, tf.float32, tf.float32)),
        num_parallel_calls=tf.data.AUTOTUNE
        )

        # Parse Test dataset:
        test_dataset = test_dataset.map(
            lambda x: tf.py_function(DataPreprocessornew.parse_function, inp=[x], Tout=(tf.float32, tf.float32)),
            num_parallel_calls=tf.data.AUTOTUNE
        )

        # Parse Valid ation dataset:
        validation_dataset = validation_dataset.map(
            lambda x: tf.py_function(DataPreprocessornew.parse_function, inp=[x], Tout=(tf.float32, tf.float32)),
            num_parallel_calls=tf.data.AUTOTUNE
        )
        
        # Create directory Datasets:
        if not os.path.exists("./Datasets"):
            os.makedirs("./Datasets")
        
        # Create folders Hospital1, Hospital2, Hospital3, FedServer, Centralized:
        if not os.path.exists("./Datasets/1"):
            os.makedirs("./Datasets/1")
        if not os.path.exists("./Datasets/2"):
            os.makedirs("./Datasets/2")
        if not os.path.exists("./Datasets/3"):
            os.makedirs("./Datasets/3")
        if not os.path.exists("./Datasets/FedServer"):
            os.makedirs("./Datasets/FedServer")
        if not os.path.exists("./Datasets/Centralized"):
            os.makedirs("./Datasets/Centralized")

        if federated == True:
            # Save the datasets as a tensorflow dataset:
            tf.data.Dataset.save(train_dataset, os.path.join(".", 'Datasets', str(hospital), 'train_dataset'))
            tf.data.Dataset.save(test_dataset, os.path.join(".", 'Datasets', str(hospital), 'test_dataset'))
            tf.data.Dataset.save(validation_dataset, os.path.join(".", 'Datasets', str(hospital), 'validation_dataset'))
            tf.data.Dataset.save(train_dataset_no_aug, os.path.join(".", 'Datasets', str(hospital), 'train_dataset_no_aug'))
        else:
            tf.data.Dataset.save(train_dataset, os.path.join(".", 'Datasets', 'Centralized', 'train_dataset'))
            tf.data.Dataset.save(test_dataset, os.path.join(".", 'Datasets', 'Centralized', 'test_dataset'))
            tf.data.Dataset.save(validation_dataset, os.path.join(".", 'Datasets', 'Centralized', 'validation_dataset'))
            tf.data.Dataset.save(train_dataset_no_aug, os.path.join(".", 'Datasets', 'Centralized', 'train_dataset_no_aug'))
            # Save the Datast for the FedServer:
            tf.data.Dataset.save(validation_dataset, os.path.join(".", 'Datasets', 'FedServer', 'validation_dataset'))
        return "Done"
        
    @staticmethod
    def load_dataset(batch_size, federated=False, hospital=None, augmentation=True):

        if federated == True:
            # Load the datasets:
            test_dataset = tf.data.Dataset.load(os.path.join(".", 'Datasets', str(hospital), 'test_dataset'))
            validation_dataset = tf.data.Dataset.load(os.path.join(".", 'Datasets', str(hospital), 'validation_dataset'))
            if augmentation == True:
                train_dataset = tf.data.Dataset.load(os.path.join(".", 'Datasets', str(hospital), 'train_dataset'))
            else:
                train_dataset = tf.data.Dataset.load(os.path.join(".", 'Datasets', str(hospital), 'train_dataset_no_aug'))
        else:
            test_dataset = tf.data.Dataset.load(os.path.join(".", 'Datasets', 'Centralized', 'test_dataset'))
            validation_dataset = tf.data.Dataset.load(os.path.join(".", 'Datasets', 'Centralized', 'validation_dataset'))
            if augmentation == True:
                train_dataset = tf.data.Dataset.load(os.path.join(".", 'Datasets', 'Centralized', 'train_dataset'))
            else:
                train_dataset = tf.data.Dataset.load(os.path.join(".", 'Datasets', 'Centralized', 'train_dataset_no_aug'))

        # Get the number of samples in each dataset:
        num_train_samples = len(list(train_dataset))

        # Cache the data for performance improvement:
        train_dataset = train_dataset.cache()
        test_dataset = test_dataset.cache()
        validation_dataset = validation_dataset.cache()

        # Shuffle the data:
        train_dataset = train_dataset.shuffle(buffer_size=1000)
        test_dataset = test_dataset.shuffle(buffer_size=1000)
        validation_dataset = validation_dataset.shuffle(buffer_size=1000)

        # Batch the data and prefetch it:
        train_dataset = train_dataset.batch(batch_size,drop_remainder=True).prefetch(buffer_size=tf.data.AUTOTUNE)
        test_dataset = test_dataset.batch(batch_size,drop_remainder=True).prefetch(buffer_size=tf.data.AUTOTUNE)
        validation_dataset = validation_dataset.batch(batch_size,drop_remainder=True).prefetch(buffer_size=tf.data.AUTOTUNE)
        
        return train_dataset, test_dataset, validation_dataset, num_train_samples

    @staticmethod
    def load_evaluationData_federated(batch_size):
        # Loads the validation dataset for the FedServer:
        validation_dataset = tf.data.Dataset.load(os.path.join(".", 'Datasets', 'FedServer', 'validation_dataset'))

        # Cache the data for performance improvement:
        validation_dataset = validation_dataset.cache()

        # Shuffle the data:
        validation_dataset = validation_dataset.shuffle(buffer_size=1000)

        # Batch the data and prefetch it:
        validation_dataset = validation_dataset.batch(batch_size, drop_remainder=True).prefetch(buffer_size=tf.data.AUTOTUNE)
        
        return validation_dataset
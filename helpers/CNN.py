import warnings
import tensorflow as tf

# Ignore warnings:
warnings.filterwarnings('ignore')


class CNNModel:

    @staticmethod
    def create_model(INPUT_SHAPE):
        """
        Creates a 1D CNN model suitable for ECG data.
        """
        # Create the model:
        model = tf.keras.Sequential()
        model.add(tf.keras.layers.Conv1D(filters=32, kernel_size=5, activation='relu', input_shape=INPUT_SHAPE))
        model.add(tf.keras.layers.BatchNormalization())
        model.add(tf.keras.layers.MaxPooling1D(pool_size=2))

        model.add(tf.keras.layers.Conv1D(filters=32, kernel_size=5, activation='relu'))
        model.add(tf.keras.layers.MaxPooling1D(pool_size=2))

        model.add(tf.keras.layers.Conv1D(filters=64, kernel_size=5, activation='relu'))
        model.add(tf.keras.layers.MaxPooling1D(pool_size=2))

        model.add(tf.keras.layers.Conv1D(filters=64, kernel_size=5, activation='relu'))
        model.add(tf.keras.layers.MaxPooling1D(pool_size=2))

        model.add(tf.keras.layers.Conv1D(filters=128, kernel_size=5, activation='relu'))
        model.add(tf.keras.layers.MaxPooling1D(pool_size=2))

        model.add(tf.keras.layers.Conv1D(filters=128, kernel_size=5, activation='relu'))
        model.add(tf.keras.layers.MaxPooling1D(pool_size=2))

        model.add(tf.keras.layers.Dropout(0.5))

        model.add(tf.keras.layers.Conv1D(filters=256, kernel_size=5, activation='relu'))
        model.add(tf.keras.layers.MaxPooling1D(pool_size=2))

        model.add(tf.keras.layers.Conv1D(filters=256, kernel_size=5, activation='relu'))
        model.add(tf.keras.layers.MaxPooling1D(pool_size=2))

        model.add(tf.keras.layers.Dropout(0.5))

        model.add(tf.keras.layers.Conv1D(filters=512, kernel_size=5, activation='relu'))
        model.add(tf.keras.layers.MaxPooling1D(pool_size=2))

        model.add(tf.keras.layers.Dropout(0.5))

        model.add(tf.keras.layers.Conv1D(filters=512, kernel_size=5, activation='relu'))
        model.add(tf.keras.layers.Flatten())

        model.add(tf.keras.layers.Dense(128, activation='relu'))
        model.add(tf.keras.layers.Dropout(0.5))

        model.add(tf.keras.layers.Dense(32, activation='relu'))
        model.add(tf.keras.layers.Dense(3, activation='softmax'))

        """model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['categorical_accuracy'])
        model.summary()"""

        return model

import tensorflow as tf
import numpy as np


class AugmentData:
    # Create a matrix of size 3x12
    A = tf.constant([[1, -1, 0], [0, 1, -1], [-1, 0, 1]], dtype=tf.float32)
    A_pseudo = tf.linalg.pinv(A)

    @staticmethod
    def transform(tensor):
        b = tensor[:, :3]
        x = tf.matmul(AugmentData.A_pseudo, b, transpose_b=True)
        return tf.transpose(x)

    @staticmethod
    def get_electrode_data(data_tensor):
        results = AugmentData.transform(data_tensor)
        results = tf.concat([results, data_tensor[:, 6:]], axis=1)
        return results

    @staticmethod
    def transform_back(electrode_data, data):
        LA, RA, LL = electrode_data[:, 0], electrode_data[:, 1], electrode_data[:, 2]
        I = LA - RA
        II = RA - LL
        III = LL - LA

        # Add the mean of the original I, II, and III leads (offset):
        mean = tf.reduce_mean(data[:, :3], axis=1)
        I, II, III = I + mean, II + mean, III + mean
        I, II, III = tf.round(I), tf.round(II), tf.round(III)

        aVR = -(I + II) // 2
        aVL = (I - III) // 2
        aVF = (II + III) // 2

        lead_data = tf.stack([I, II, III, aVR, aVL, aVF], axis=1)
        lead_data = tf.concat([lead_data, electrode_data[:, 3:]], axis=1)
        return lead_data

    @staticmethod
    def set_random_electrode_to_zero(electrode_data, data):
        # Get a random integer between 0 and 8 (inclusive):
        electrode_index = np.random.randint(0, electrode_data.shape[-1])

        # Create a mask tensor where all elements are 1 except for the selected electrode_index which is 0
        mask = tf.ones_like(electrode_data, dtype=tf.float32)
        row_indices = tf.range(electrode_data.shape[0], dtype=tf.int32)
        update_indices = tf.stack([row_indices, tf.ones_like(row_indices) * electrode_index], axis=-1)
        mask = tf.tensor_scatter_nd_update(mask, indices=update_indices, updates=tf.zeros(electrode_data.shape[0]))

        # Multiply the electrode_data by the mask tensor to set the selected column to zero
        modified_electrode_data = tf.multiply(electrode_data, mask)

        # Transform the modified electrode data back to the original leads
        modified_electrode_data = AugmentData.transform_back(modified_electrode_data, data)
        return modified_electrode_data

    @staticmethod
    def add_gaussian_noise(electrode_data, noise_fraction, data):
        electrode_index = np.random.randint(0, electrode_data.shape[-1])

        # Standard deviation of the chosen electrode:
        standard_dev = tf.math.reduce_std(electrode_data[:, electrode_index])


        # Calculate the standard deviation of the Gaussian noise based on the noise_fraction
        noise_std_dev = noise_fraction * standard_dev
        noise = tf.random.normal(shape=tf.shape(electrode_data[:, electrode_index]), mean=0, stddev=noise_std_dev)
        # Add the noise to the chosen electrode
        updated_column = electrode_data[:, electrode_index] + noise
        electrode_data = tf.concat([electrode_data[:, :electrode_index], tf.expand_dims(updated_column, axis=-1),
                                    electrode_data[:, electrode_index + 1:]], axis=-1)

        # Transform the modified electrode data back to the original leads
        transform_back = AugmentData.transform_back(electrode_data, data)

        return transform_back

    @staticmethod
    def add_gaussian_noise_snr(electrode_data, snr, data):
        # Choose a random electrode:
        electrode_index = np.random.randint(0, electrode_data.shape[-1])

        # Calculate the signal power
        sp = tf.math.reduce_mean(tf.math.square(electrode_data[:, electrode_index]))

        # Calculate the noise standard deviation based on the desired SNR
        noise_std_dev = tf.math.sqrt(sp / snr)

        # Generate Gaussian noise with the calculated standard deviation
        noise = tf.random.normal(shape=tf.shape(electrode_data[:, electrode_index]), mean=0, stddev=noise_std_dev)

        # Add the noise to the chosen electrode
        updated_column = electrode_data[:, electrode_index] + noise
        electrode_data = tf.concat([electrode_data[:, :electrode_index], tf.expand_dims(updated_column, axis=-1),
                                    electrode_data[:, electrode_index + 1:]], axis=-1)

        # Transform the modified electrode data back to the original leads
        transform_back = AugmentData.transform_back(electrode_data, data)

        return transform_back

    @staticmethod
    def generate_baseline_wander(electrode_data, freq_range=(0.05, 0.5), amplitude_range=(0.5, 1.5)):
        sampling_rate = 500
        data_length = tf.shape(electrode_data)[0]
        time = tf.range(data_length, dtype=tf.float32) / sampling_rate
        min_freq, max_freq = freq_range
        min_amplitude, max_amplitude = amplitude_range
        baseline_wander = tf.zeros(data_length, dtype=tf.float32)

        num_components = np.random.randint(2, 10)

        for component in range(num_components):
            frequency = np.random.uniform(min_freq, max_freq)
            amplitude = np.random.uniform(min_amplitude, max_amplitude)
            phase = np.random.uniform(0, 2 * np.pi)
            baseline_wander += amplitude * tf.math.sin(2 * np.pi * frequency * time + phase)

        return baseline_wander

    @staticmethod
    def add_baseline_wander(electrode_data, data, freq_range=(0.05, 0.5), amplitude_range=(0.5, 1.5)):
        # Generate the baseline wander signal
        baseline_wander = AugmentData.generate_baseline_wander(electrode_data, freq_range, amplitude_range)
        # Choose a random electrode
        electrode_index = np.random.randint(0, electrode_data.shape[-1])
        # Add the baseline wander signal to the chosen electrode
        updated_column = electrode_data[:, electrode_index] + baseline_wander
        electrode_data = tf.concat([electrode_data[:, :electrode_index], tf.expand_dims(updated_column, axis=-1),
                                    electrode_data[:, electrode_index + 1:]], axis=-1)

        # Transform the modified electrode data back to the original leads
        transform_back = AugmentData.transform_back(electrode_data, data)

        return transform_back

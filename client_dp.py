import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
import argparse
import flwr as fl
import numpy as np
import tensorflow as tf
from helpers.CNN import CNNModel
from helpers.DataPreprocessornew import DataPreprocessornew
from tensorflow_privacy.privacy.optimizers.dp_optimizer_keras import DPKerasAdamOptimizer
from helpers.dp import compute_epsilon

# global for tracking privacy
PRIVACY_LOSS = 0

# Create a ECG client for Federated Learning:
class ECGClient(fl.client.NumPyClient):
    def __init__(self, traindata, testdata, valdata, num_train, args):
        self.traindata = traindata
        self.testdata = testdata
        self.valdata = valdata
        self.num_train = num_train
        self.input_shape = (5000, 12)
        self.model = CNNModel.create_model(self.input_shape)
        self.batch_size = args.batch_size
        self.local_epochs = args.local_epochs
        self.dp = args.dp

        if args.dp:
            self.noise_multiplier = args.noise_multiplier
            optimizer = DPKerasAdamOptimizer(
                l2_norm_clip=args.l2_norm_clip,
                noise_multiplier=args.noise_multiplier,
                num_microbatches=16,
            )
            # Conpute per sample loss:
            loss = tf.keras.losses.CategoricalCrossentropy(reduction=tf.losses.Reduction.NONE)
            self.model.compile(optimizer=optimizer, loss=loss, metrics=["categorical_accuracy"])

        else:
            self.model.compile("adam", loss="categorical_crossentropy", metrics=["categorical_accuracy"])

    def get_parameters(self):
        """Get parameters of the local model."""
        raise Exception("Not implemented (server-side parameter initialization)")

    def fit(self, parameters, config):
        """Train parameters on the locally held training set."""
        global PRIVACY_LOSS
        # Update local model parameters:
        if self.dp:
            pass
            privacy_spent = compute_epsilon(
                self.local_epochs,
                self.num_train,
                self.batch_size,
                self.noise_multiplier,
            )
            PRIVACY_LOSS += privacy_spent

        self.model.set_weights(parameters)
        self.model.fit(
            self.traindata, 
            epochs=self.local_epochs,
            batch_size=self.batch_size,
            validation_data=self.valdata)
        
        return self.model.get_weights(), self.num_train, {}

    
    def evaluate(self, parameters, config):
        """Evaluate parameters on the locally held test set."""
        self.model.set_weights(parameters)
        y_values = np.concatenate([y for x, y in self.testdata], axis=0)
        y_values = np.argmax(y_values, axis=1)
        loss, accuracy = self.model.evaluate(self.testdata)
        num_examples_test = len(y_values)
        return loss, num_examples_test, {"accuracy": accuracy}


def main(args) -> None:

    traindata, testdata, valdata, num_train = DataPreprocessornew.load_dataset(batch_size=args.batch_size, federated=True, hospital=args.hospital, augmentation=args.augmentation)
    client = ECGClient(traindata, testdata, valdata, num_train, args)

    fl.client.start_numpy_client(
        server_address="127.0.0.1:8980",
        client=client
    )
    if args.dp:
        print("Privacy Loss: ", PRIVACY_LOSS)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="FlowerClients")
    parser.add_argument("--hospital", type=int, required=True)
    parser.add_argument("--augmentation", type=bool, default=True)
    parser.add_argument("--local-epochs", type=int, default=1)
    parser.add_argument("--batch-size", type=int, default=16)
    parser.add_argument("--dp", type=bool, default=True, help="Differential privacy")
    parser.add_argument("--l2-norm-clip", default=1.5, type=float, help="Clipping norm")
    parser.add_argument("--noise-multiplier", default=1.3, type=float, help="Ratio of the standard deviation to the clipping norm")
    args = parser.parse_args()
    main(args)
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf
from helpers.CNN import CNNModel
from helpers.DataPreprocessornew import DataPreprocessornew
from sklearn.metrics import classification_report
import numpy as np


class Trainer:
    def __init__(self, input_dir, batch_size, num_epochs, federated=False, hospital=None, CNN=True, augmentation=False):
        self.CNN = CNN
        self.augmentation = augmentation
        self.input_dir = input_dir
        self.batch_size = batch_size
        self.num_epochs = num_epochs
        self.federated = federated
        self.augmentation = augmentation
        self.hospital = hospital
        self.input_shape = (5000, 12)
        self.model = CNNModel.create_model(self.input_shape)
        self.model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['categorical_accuracy'])
        self.train_dataset = None
        self.test_dataset = None
        self.early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=5)

    def train(self):
        self.train_dataset, self.testing, self.valdation, num_train = DataPreprocessornew.load_dataset(self.batch_size, self.federated, self.hospital, self.augmentation)
        history = self.model.fit(self.train_dataset, epochs=self.num_epochs, validation_data=self.valdation,
                                 batch_size=self.batch_size, callbacks=[self.early_stopping], verbose=2)
        
        # Evaluate the model on the test data using `evaluate`
        print("Evaluate on test data")
        y_true = []
        y_pred = []

        for x, y in self.testing:
            batch_pred = self.model.predict(x)
            batch_pred = np.argmax(batch_pred, axis=1)
            y = np.argmax(y, axis=1)

            y_true.extend(y.tolist())
            y_pred.extend(batch_pred.tolist())

        report = classification_report(y_true, y_pred)

        # Create folder evaluation if it does not exist
        if not os.path.exists('evaluation/centralised'):
            os.makedirs('evaluation/centralised')

        # Check if the model folder exists
        if not os.path.exists('models'):
            os.makedirs('models')
            
        # Save the classification report in the evaluation folder
        if self.augmentation and self.CNN:
            # Save the model in the model folder:
            self.model.save('models/model_augmented_CNN')

            with open('evaluation/centralised/classification_report_augmented_CNN.txt', 'w') as f:
                f.write(report)

        elif self.augmentation and not self.CNN:
            with open('evaluation/centralised/classification_report_augmented_ResNet.txt', 'w') as f:
                f.write(report)
        
        elif not self.augmentation and self.CNN:
            with open('evaluation/centralised/classification_report_CNN.txt', 'w') as f:
                f.write(report)

        elif not self.augmentation and not self.CNN:
            with open('evaluation/centralised/classification_report_ResNet.txt', 'w') as f:
                f.write(report)

        print(report)

    
if __name__ == '__main__':
    input_dir = "/home/dennis/GitHub/ECG/data"
    batch_size = 256
    num_epochs = 40
    federated = False
    hospital = None
    trainer = Trainer(input_dir, batch_size, num_epochs, federated, hospital, CNN=True, augmentation=True)
    trainer.train()

import argparse
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
from helpers.CNN import CNNModel
from typing import Any, Callable, Dict, List, Optional, Tuple
from helpers.DataPreprocessornew import DataPreprocessornew
from helpers.dp import compute_epsilon
import flwr as fl
import tensorflow as tf


def get_eval_fn(model, batch_size):
    """Returns an evaluation function for server-side evaluation."""
    eval_data = DataPreprocessornew.load_evaluationData_federated(batch_size=batch_size)

    def evaluate(
        server_round: int,
        weights: fl.common.NDArrays,
        config: Dict[str, fl.common.Scalar],
    ) -> Optional[Tuple[float, Dict[str, fl.common.Scalar]]]:
        model.set_weights(weights)  
        loss, accuracy = model.evaluate(eval_data)
        return loss, {"accuracy": accuracy}
    return evaluate


def main(args) -> None:

    model = CNNModel.create_model((5000, 12))
    loss = tf.keras.losses.CategoricalCrossentropy(from_logits=True)
    model.compile("adam", loss=loss, metrics=["categorical_accuracy"])
    
    # Define the flower strategy:
    strategy = fl.server.strategy.FedAvg(
        fraction_fit=1,
        fraction_evaluate=1,
        min_fit_clients=3,
        min_evaluate_clients=3,
        min_available_clients=3,
        evaluate_fn=get_eval_fn(model, args.batch_size),
        initial_parameters=fl.common.ndarrays_to_parameters(model.get_weights()),
    )

    # Start Flower server (SSL-enabled) for four rounds of federated learning
    fl.server.start_server(
        server_address="localhost:8980",
        config=fl.server.ServerConfig(num_rounds=args.num_rounds),
        strategy=strategy,
    )

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Server Script")
    parser.add_argument("--num-rounds", default=2, type=int)
    parser.add_argument("--batch-size", type=int, default=16)
    args = parser.parse_args()
    main(args)
